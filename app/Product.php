<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //public $connection = 'sqlsrv2';

    protected $fillable = [
        'name', 'description',
    ];
}
