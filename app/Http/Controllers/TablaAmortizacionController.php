<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TablaAmortizacion;
use Illuminate\Support\Facades\DB;

class TablaAmortizacionController extends Controller
{
    public function index($clienteId)
    {
        // $tablaAmortizacion = DB::select('drop table users');
        // $query = TablaAmortizacion::where('cliente_id', $clienteId)->with('cliente')->toSql();
        // dd($query);

        $tablaAmortizacion = TablaAmortizacion::where('cliente_id', $clienteId)->with('cliente')->paginate(15);

        $titulo = 'Tabla de amortización';

        return view('TablaAmortizacion.index')->with(['tablaAmortizacion' => $tablaAmortizacion, 'title' => $titulo]);
    }
}
